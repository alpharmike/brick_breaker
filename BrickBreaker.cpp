#include "BrickBreaker.h"
#include <iostream>
#include <gl\glut.h>
#include <cmath>

#define BRICK 999
#define RACQUET 1000
#define BALL 1001
#define FIELD 1002

using namespace std;

BrickBreaker::GameState brickBreakerGame(1920, 1080);

void BrickBreaker::GameState::init()
{
    this -> started = false;

    this->Player_1.Position = (this->FieldHeight - this->RaquetHeight) / 2;
    this->Player_2.Position = (this->FieldWidth - this->RaquetWidth) / 2;

    this->Ball.Position_X = this->Player_2.Position + this->RaquetWidth / 2;
    this->Ball.Position_Y = this->RaquetHeight + BallSize / 2;

    auto theta = ((double)rand() / (RAND_MAX)) * 360;
    this->Ball.Velocity_X = 0;
    this->Ball.Velocity_Y = 0;

    bricks.clear();

    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 20; ++j) {
            int x = (j * BrickWidth);
            int y = FieldHeight - i * BrickHeight;
            Brick brick = {
                    x, y
            };
            float r = rand() / (float) RAND_MAX;
            float g = rand() / (float) RAND_MAX;
            float b = rand() / (float) RAND_MAX;

            brick.color.push_back(r <= 0.1 ? r + 0.1 : r);
            brick.color.push_back(g <= 0.1 ? g + 0.1 : g);
            brick.color.push_back(b <= 0.1 ? b + 0.1 : b);
            bricks.push_back(brick);
        }
    }

//    for (int i = 0; i < BrickCount; ++i) {
//        int x = rand() % (FieldWidth - BrickWidth - 0);
//        int y = rand() % (FieldHeight - BrickHeight - 0);
//        Brick brick = {
//                x, y
//        };
//        bool collision = false;
//
//        for (int j = 0; j < bricks.size(); ++j) {
//            Brick currBrick = bricks.at(j);
//            if (BrickCollision(currBrick, brick)) {
//                collision = true;
//                break;
//            }
//        }
//
//        if (collision || brick.Position_Y <= RaquetHeight) {
//            --i;
//            continue;
//        }
//        float r = rand() / (float )RAND_MAX;
//        float g = rand() / (float)RAND_MAX;
//        float b = rand() / (float)RAND_MAX;
//
//        brick.color.push_back(r <= 0.1 ? r + 0.1 : r);
//        brick.color.push_back(g <= 0.1 ? g + 0.1 : g);
//        brick.color.push_back(b <= 0.1 ? b + 0.1 : b);
//        bricks.push_back(brick);
//    }
}

void BrickBreaker::GameState::change_state(int player, int state)
{
    auto W = glutGet(GLUT_WINDOW_WIDTH);

//    printf("WIDTH: %d   ", state);

    this->Player_2.Position =
                fmax(fmin((double)state / W * this->FieldWidth, this->FieldWidth - this->RaquetWidth), 0);

    if (!this->started) {
        this->Ball.Position_X = this->Player_2.Position + this->RaquetWidth / 2;
        this->Ball.Position_Y = this->RaquetHeight + BallSize / 2;
    }

//    printf("%d", this->Player_2.Position);


    glutPostRedisplay();
}

//bool BrickBreaker::Collision(int x0, int y0, int w0, int h0, int x1, int y1, int w1, int h1)
//{
//    auto l = fmax(x0, x1);
//    auto t = fmin(y0, y1);
//
//    auto r0 = x0 + w0, r1 = x1 + w1;
//    auto b0 = y0 - h0, b1 = y1 - h1;
//
//    auto b = fmax(b0, b1);
//    auto r = fmin(r0, r1);
//
//    return l <= r && t >= b;
//}

bool BrickBreaker::BrickCollision(BrickBreaker::Brick firstBrick, BrickBreaker::Brick secondBrick) {
    int width = brickBreakerGame.BrickWidth;
    int height = brickBreakerGame.BrickHeight;
    bool X_Collision = firstBrick.Position_X + width >= secondBrick.Position_X && secondBrick.Position_X + width >= firstBrick.Position_X;
    bool Y_Collision = firstBrick.Position_Y + height >= secondBrick.Position_Y && secondBrick.Position_Y + height >= firstBrick.Position_Y;

    return X_Collision && Y_Collision;
}

bool BrickBreaker::Collision(int x0, int y0, int w0, int h0, int x1, int y1, int w1, int h1)
{
    auto l = fmax(x0, x1);
    auto t = fmin(y0, y1);

    auto r0 = x0 + w0, r1 = x1 + w1;
    auto b0 = y0 - h0, b1 = y1 - h1;

    auto b = fmax(b0, b1);
    auto r = fmin(r0, r1);

    return l <= r && t >= b;
}

void BrickBreaker::GameState::next_state()
{
    Ball.Position_X += Ball.Velocity_X * 10;
    Ball.Position_Y += Ball.Velocity_Y * 10;

    if (Ball.Position_X - BallSize <= 0)
    {
        Ball.Position_X = BallSize;
        Ball.Velocity_X *= -1;
    }
    else if (Ball.Position_X + 2 * BallSize >= this->FieldWidth)
    {
        Ball.Position_X = this->FieldWidth - 2 * BallSize;
        Ball.Velocity_X *= -1;
    }
    else if (Ball.Position_Y + BallSize >= this->FieldHeight)
    {
        Ball.Position_Y = this->FieldHeight - BallSize;
        Ball.Velocity_Y *= -1;
    }

    if (Ball.Position_X + BallSize / 2 >= brickBreakerGame.Player_2.Position && Ball.Position_X - BallSize / 2 <= brickBreakerGame.Player_2.Position + brickBreakerGame.RaquetWidth && Ball.Position_Y - BallSize / 2 <= brickBreakerGame.RaquetHeight) {
        Ball.Velocity_Y *= -1;
    }

    for (int i = 0; i < this->bricks.size(); ++i) {
        Brick currBrick = this->bricks.at(i);
        if ((this->Ball.Position_X + 2 * BallSize >= currBrick.Position_X && this->Ball.Position_X <= currBrick.Position_X + this->BrickWidth) && (this->Ball.Position_Y + 2 *BallSize >= currBrick.Position_Y && this->Ball.Position_Y - 2 * BallSize <= currBrick.Position_Y + this->BrickHeight)) {
//            printf("Count: %d", bricks.size());
            this->Ball.Velocity_Y *= -1;
            bricks.erase(bricks.begin() + i);
        }
    }

    if (Ball.Position_Y < -BallSize * 2 || bricks.empty())
    {
        init();
    }

    glutPostRedisplay();
    glutTimerFunc(20, BrickBreaker::timer, 0);
}

void BrickBreaker::keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 'q':
        case 'Q':
            exit(0);
            break;
        case 'i':
        case 'I':
            brickBreakerGame.init();
            break;
    }
}

void BrickBreaker::keyboard_control(int key, int x, int y)
{
    switch (key)
    {
        case GLUT_KEY_UP:
            brickBreakerGame.change_state(1, 1);
            break;
        case GLUT_KEY_DOWN:
            brickBreakerGame.change_state(1, -1);
            break;
        default:
            brickBreakerGame.change_state(1, 0);
            break;
    }
}

void BrickBreaker::keyboard_up(int key, int x, int y)
{
    brickBreakerGame.change_state(1, 0);
}

void BrickBreaker::mouse(int button, int state, int x, int y)
{
    if (!brickBreakerGame.started) {
        brickBreakerGame.started = true;
        auto theta = ((double)rand() / (RAND_MAX)) * 360;
        brickBreakerGame.Ball.Velocity_X = cos(theta) * 2;
        brickBreakerGame.Ball.Velocity_Y = abs(sin(theta)) * 2;
    }
    cout << x << " , " << y << endl;
}

void BrickBreaker::motion(int x, int y)
{
    brickBreakerGame.change_state(2, x);
}

void BrickBreaker::timer(int value)
{
    brickBreakerGame.next_state();
}

void BrickBreaker::render()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glCallList(FIELD);

    glPushMatrix();
    glTranslatef(brickBreakerGame.Player_2.Position,0, 0);
    glCallList(RACQUET);
    glPopMatrix();

    for (int i = 0; i < brickBreakerGame.bricks.size(); ++i) {
        glPushMatrix();
        Brick currBrick = brickBreakerGame.bricks.at(i);
        glTranslatef(currBrick.Position_X, currBrick.Position_Y, 0);
        float color[] = {currBrick.color.at(0), currBrick.color.at(1), currBrick.color.at(2),};
        glColor3fv(color);
        glCallList(BRICK);
        glPopMatrix();
    }


    glPushMatrix();
    glTranslatef(brickBreakerGame.Ball.Position_X, brickBreakerGame.Ball.Position_Y, 0);
    glCallList(BALL);
    glPopMatrix();

    glutSwapBuffers();
}

void BrickBreaker::init()
{
    brickBreakerGame.init();

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, 1920, 0, 1080);

    glNewList(FIELD, GL_COMPILE);
    Lines::Bresenham(0, 0, brickBreakerGame.FieldWidth, 0);
    Lines::Bresenham(brickBreakerGame.FieldWidth - 1, 0, brickBreakerGame.FieldWidth - 1, brickBreakerGame.FieldHeight);
    Lines::Bresenham(0, brickBreakerGame.FieldHeight - 1, brickBreakerGame.FieldWidth, brickBreakerGame.FieldHeight - 1);
    Lines::Bresenham(0, 0, 0, brickBreakerGame.FieldHeight);
    glEndList();

    glNewList(RACQUET, GL_COMPILE);
    glColor3f(0.3, 0.5, 9.0);
    glBegin(GL_POLYGON);
    glVertex2i(0, 0);
    glVertex2i(brickBreakerGame.RaquetWidth, 0);
    glVertex2i(brickBreakerGame.RaquetWidth, brickBreakerGame.RaquetHeight);
    glVertex2i(0, brickBreakerGame.RaquetHeight);
    glEnd();
    glEndList();

    glNewList(BRICK, GL_COMPILE);
    glBegin(GL_POLYGON);
        glVertex2i(0, 0);
        glVertex2i(brickBreakerGame.BrickWidth, 0);
        glVertex2i(brickBreakerGame.BrickWidth, brickBreakerGame.BrickHeight);
        glVertex2i(0, brickBreakerGame.BrickHeight);
    glEnd();
    glEndList();

    glNewList(BALL, GL_COMPILE);
    Circles::MidPoint(brickBreakerGame.BallSize / 2, brickBreakerGame.BallSize / 2, brickBreakerGame.BallSize);
    glEndList();
}

void BrickBreaker::reshape(int width, int height)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    auto aspectRatio = (float)brickBreakerGame.FieldWidth / brickBreakerGame.FieldHeight;
    gluOrtho2D(0, brickBreakerGame.FieldWidth, -brickBreakerGame.FieldWidth / aspectRatio / 2 + brickBreakerGame.FieldHeight / 2, brickBreakerGame.FieldWidth / aspectRatio / 2 + brickBreakerGame.FieldHeight / 2);

    glViewport(5, 5, width - 10, height - 10);
}

void BrickBreaker::main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

    glutInitWindowSize(800, 600);
    glutCreateWindow("Ping Pong");
    glutFullScreen();

    BrickBreaker::init();

    glutDisplayFunc(BrickBreaker::render);
    glutReshapeFunc(BrickBreaker::reshape);
    glutKeyboardFunc(BrickBreaker::keyboard);
    glutSpecialUpFunc(BrickBreaker::keyboard_up);
    glutSpecialFunc(BrickBreaker::keyboard_control);
    glutMouseFunc(BrickBreaker::mouse);
    glutPassiveMotionFunc(BrickBreaker::motion);
    glutTimerFunc(20, BrickBreaker::timer, 0);

    glutMainLoop();
}