//
// Created by Sandman on 10/30/2020.
//

#ifndef BOUNCINGBALL_BRICKBREAKER_H
#define BOUNCINGBALL_BRICKBREAKER_H

#pragma once
#include "Circles.h"
#include "Lines.h"
#include <vector>
#include <random>
#include <gl/glut.h>

#define M_PI 3.14159265358979323846

namespace BrickBreaker
{
    struct Player
    {
        int Position;
    };

    struct Ball
    {
        int Position_X;
        int Position_Y;
        float Velocity_X;
        float Velocity_Y;
    };

    struct Brick {
        int Position_X;
        int Position_Y;
        std::vector<GLfloat> color;
    };

    struct GameState
    {
        int FieldWidth;
        int FieldHeight;
        int RaquetWidth;
        int RaquetHeight;
        int RaquetMargin;
        int BallSize;
        int BrickCount;
        int BrickWidth;
        int BrickHeight;
        bool started;
        std::vector<Brick> bricks;

        Player Player_1;
        Player Player_2;
        Ball Ball;

        GameState(int width, int height) :
                FieldWidth(width), FieldHeight(height)
        {
            RaquetWidth = width / 4;
            RaquetHeight = height / 16;
            BallSize = height / 24;
            RaquetMargin = width / 24;
            BrickCount = 10;
            BrickWidth = 150;
            BrickHeight = 100;

//            for (int i = 0; i < BrickCount; ++i) {
//                int x = rand() % (width - BrickWidth - 0);
//                int y = rand() % (height - BrickHeight - 0);
//                float r = rand() / (float )RAND_MAX;
//                float g = rand() / (float)RAND_MAX;
//                float b = rand() / (float)RAND_MAX;
//                Brick brick = {
//                        x, y
//                };
//                brick.color.push_back(r <= 0.1 ? r + 0.1 : r);
//                brick.color.push_back(g <= 0.1 ? g + 0.1 : g);
//                brick.color.push_back(b <= 0.1 ? b + 0.1 : b);
//                bricks.push_back(brick);
//            }
        }

        void init();
        void next_state();
        void change_state(int player, int state);
    };

    void init();
    void render();
    void reshape(int width, int height);
    void keyboard(unsigned char key, int x, int y);
    void keyboard_control(int key, int x, int y);
    void keyboard_up(int key, int x, int y);
    void mouse(int button, int state, int x, int y);
    void motion(int x, int y);
    void timer(int value);
    bool Collision(int x0, int y0, int w0, int h0, int x1, int y1, int w1, int h1);
    bool BrickCollision(Brick firstBrick, Brick secondBrick);

    void main(int argc, char** argv);
}

#endif //BOUNCINGBALL_BRICKBREAKER_H
